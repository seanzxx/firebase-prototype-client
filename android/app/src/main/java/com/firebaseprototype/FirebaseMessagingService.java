package com.firebaseprototype;

import android.content.Intent;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;

/**
 * Created by seanzxx on 7/11/16.
 */
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Intent intent = new Intent("com.firebaseprototype.NEW_MESSAGE");
        intent.putExtra("messageData", new HashMap(remoteMessage.getData()));

        sendBroadcast(intent);
    }
}