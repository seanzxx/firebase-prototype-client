package com.firebaseprototype;

import android.content.Intent;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by seanzxx on 7/11/16.
 */
public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        Intent intent = new Intent("com.firebaseprototype.NEW_DEVICE_TOKEN");
        intent.putExtra("token", FirebaseInstanceId.getInstance().getToken());
        sendBroadcast(intent);
    }
}
