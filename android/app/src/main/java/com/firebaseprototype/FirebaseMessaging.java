package com.firebaseprototype;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.facebook.react.bridge.*;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by seanzxx on 8/4/16.
 */
public class FirebaseMessaging extends ReactContextBaseJavaModule {

    public FirebaseMessaging(final ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public void initialize() {
        super.initialize();

        getReactApplicationContext().registerReceiver(NEW_MESSAGE_RECEIVER, NEW_MESSAGE_INTENT_FILTER);
        getReactApplicationContext().registerReceiver(NEW_DEVICE_TOKEN_RECEIVER, NEW_DEVICE_TOKEN_INTENT_FILTER);

        getReactApplicationContext().addActivityEventListener(FIREBASE_ACTIVITY_EVENT_LISTENER);
    }

    @Override
    public void onCatalystInstanceDestroy() {
        super.onCatalystInstanceDestroy();

        getReactApplicationContext().unregisterReceiver(NEW_MESSAGE_RECEIVER);
        getReactApplicationContext().unregisterReceiver(NEW_DEVICE_TOKEN_RECEIVER);

        getReactApplicationContext().removeActivityEventListener(FIREBASE_ACTIVITY_EVENT_LISTENER);
    }

    @Override
    public String getName() {
        return "FirebaseMessaging";
    }

    @ReactMethod
    public void getDeviceToken(Promise promise) {
        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        promise.resolve(deviceToken);
    }

    @ReactMethod
    public void processPendingBackgroundMessages() {
        if (getCurrentActivity() != null) {
            handleNewBackgroundMessage(getCurrentActivity().getIntent());
        }
    }

    private final IntentFilter NEW_DEVICE_TOKEN_INTENT_FILTER = new IntentFilter("com.firebaseprototype.NEW_DEVICE_TOKEN");
    private final BroadcastReceiver NEW_DEVICE_TOKEN_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String token = intent.getStringExtra("token");

            getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class).emit("com.firebaseprototype.NEW_DEVICE_TOKEN", token);
        }
    };

    private final IntentFilter NEW_MESSAGE_INTENT_FILTER = new IntentFilter("com.firebaseprototype.NEW_MESSAGE");
    private final BroadcastReceiver NEW_MESSAGE_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Map<String, String> messageData = (Map<String, String>) intent.getSerializableExtra("messageData");

            handleNewMessage(messageData);
        }
    };

    private final ActivityEventListener FIREBASE_ACTIVITY_EVENT_LISTENER = new BaseActivityEventListener() {
        @Override
        public void onNewIntent(Intent intent) {
            handleNewBackgroundMessage(intent);
        }
    };

    private void handleNewBackgroundMessage(Intent intent) {
        if (intent != null && intent.hasExtra("fromUser") && intent.hasExtra("content")) {
            Map<String, String> messageData = new HashMap<>();
            messageData.put("fromUser", intent.getStringExtra("fromUser"));
            messageData.put("content", intent.getStringExtra("content"));
            messageData.put("messageSource", "intent");

            handleNewMessage(messageData);
        }
    }

    private void handleNewMessage(Map<String, String> messageData) {
        WritableMap map = Arguments.createMap();
        for (Map.Entry<String, String> e : messageData.entrySet()) {
            map.putString(e.getKey(), e.getValue());
        }

        getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class).emit("com.firebaseprototype.NEW_MESSAGE", map);
    }
}
