import React, { Component } from 'react';
import { ScrollView, StyleSheet} from 'react-native';

import ConversationListItem from './conversation-list-item';

export default class ConversationList extends Component {
    constructor(props) {
        super(props);

        this.props = props;
    }

    render() {

        return (
            <ScrollView>
                {Object.keys(this.props.conversations)
                    .map((user) => Object.assign(this.props.conversations[user].slice(-1)[0] || {}, {"user":user}))
                    .map(({user, content})=><ConversationListItem onPress={()=>this.props.onSelect(user)} key={user} fromUser={user} content={content}/>)
                }
            </ScrollView>
        )
    }
};
