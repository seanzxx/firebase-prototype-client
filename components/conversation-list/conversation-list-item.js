import React, { Component } from 'react';
import { View, Text, TouchableHighlight, StyleSheet} from 'react-native';

export default class ConversationListItem extends React.Component {
    constructor(props) {
        super(props);

        this.props = props;
    }

    render() {
        return (
            <TouchableHighlight style={styles.listItem} underlayColor="#B5B5B5" onPress={this.props.onPress}>
                <View>
                    <Text style={styles.fromUser}>{this.props.fromUser}</Text>
                    <Text style={styles.content}>{this.props.content}</Text>
                </View>
            </TouchableHighlight>
        );
    }
};

const styles = StyleSheet.create({
    listItem: {
        backgroundColor: 'white',
        padding: 15,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#CDCDCD',
    },
    fromUser: {
        fontWeight: 'bold',
    },
    content: {

    }
});
