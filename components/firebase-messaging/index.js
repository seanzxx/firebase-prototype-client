import {NativeModules, DeviceEventEmitter} from 'react-native';

class FirebaseMessaging {
  addEventListener(name, callback) {
    switch (name) {
      case 'newMessage':
        DeviceEventEmitter.addListener('com.firebaseprototype.NEW_MESSAGE', callback);
        break;
      case 'newDeviceToken':
        DeviceEventEmitter.addListener('com.firebaseprototype.NEW_DEVICE_TOKEN', callback);
        break;
    }
  }

  async getDeviceToken() {
    return await NativeModules.FirebaseMessaging.getDeviceToken();
  }

  processPendingBackgroundMessages() {
    NativeModules.FirebaseMessaging.processPendingBackgroundMessages();
  }
}

export default new FirebaseMessaging();
