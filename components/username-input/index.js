import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet} from 'react-native';

export default class UsernameInput extends Component {

  constructor(props) {
    super(props);

    this.props = props;
  }

  render() {
    return (
        <TextInput placeholder='Please input a username'
                   onSubmitEditing={(event)=>{this.props.onSubmit(event.nativeEvent.text)}}
        />
    )
  }
};


const styles = StyleSheet.create({

});
