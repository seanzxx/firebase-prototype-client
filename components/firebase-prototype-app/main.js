import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Navigator,
} from 'react-native';

import ConversationList from '../conversation-list';
import ConversationView from '../conversation-view';

export default class Main extends Component {
    constructor(props) {
        super(props);

        this.props = props;
    }

    render() {
        return (
            <Navigator
                initialRoute={{index: 0, title: 'Conversation List'}}
                renderScene={this.renderScene}
                navigationBar={
                    <Navigator.NavigationBar
                        routeMapper={{
                            LeftButton: (route, navigator, index, navState) => (route.index == 0 ? null : <TouchableHighlight onPress={() => navigator.pop() }><Text> Back</Text></TouchableHighlight>),
                            Title: (route, navigator, index, navState) => (<Text style={styles.navigationBarTitle}>{route.title}</Text>),
                            RightButton: (route, navigator, index, navState) => null
                        }}
                        style={styles.navigationBar}/>
                }
                />
        );
    }

    renderScene = (route, navigator) => 
        <View style={styles.scene}>
            {
                route.index == 0 ?
                    <ConversationList conversations={this.props.conversations} onSelect={(user) => navigator.push({ index: 1, title: user, fromUser: user }) }/> :
                    <ConversationView conversation={this.props.conversations[route.fromUser] || []} onSend={(content) => { this.props.onSend(route.fromUser, content) } }/>
            }
        </View>
}

const styles = StyleSheet.create({
    scene: {
        marginTop: 50,
        flex: 1,
    },
    navigationBar: {
        backgroundColor: '#007acc',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    navigationBarTitle: {
        color: 'white',
    }
});