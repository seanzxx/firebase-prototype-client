import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    TextInput,
    Navigator,
    AsyncStorage,
} from 'react-native';

import firebaseMessaging from '../firebase-messaging';
import firebasePrototypeServer from '../firebase-prototype-server';

import Login from './login';
import Main from './main';

export default class FirebasePrototypeApp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: null,
            token: '',
            conversations: {}
        };
    }

    componentWillMount() {
        this.initializeUsername();
        this.initializeUsernameList();

        firebaseMessaging.addEventListener('newMessage', (message) => {
            this.processMessage(message.fromUser, message);
        });
        firebaseMessaging.processPendingBackgroundMessages();
    }

    processMessage(user, message) {
        this.setState({
            ...this.state,
            conversations: {
                ...this.state.conversations,
                [user]:[
                    ...(this.state.conversations[user] || []),
                    message
                ]
            }
        });
    }

    async initializeUsername() {
        var username = await AsyncStorage.getItem('username');
        if (username != null) {
            this.setState({...this.state, username: username});
            this.changeUsername(username);
        }
    }

    async initializeUsernameList() {
      let usernames = await firebasePrototypeServer.getUsernames();
      usernames.forEach((name)=>{
        if (!this.state.conversations[name]) {
          this.state.conversations[name] = [];
        }
      });
    }

    sendMessage = async (toUser, content) => {
      if (firebasePrototypeServer.newMessage(this.state.username, toUser, content)) {
          this.processMessage(toUser, {
            fromUser: this.state.username,
            user: toUser,
            content: content,
          })
      }
    }

    changeUsername = async (username) => {
      var token = await firebaseMessaging.getDeviceToken();
      firebasePrototypeServer.refreshDeviceToken(username, token);
      AsyncStorage.setItem('username', username);

      this.setState({
        ...this.state,
        'username': username,
        'token:': token
      })
    }

    render() {
      return this.state.username ? 
               <Main conversations={this.state.conversations} onSend={this.sendMessage} initialUsername={this.state.initialUsername} /> : 
               <Login onLogin = {this.changeUsername} />
    }
}