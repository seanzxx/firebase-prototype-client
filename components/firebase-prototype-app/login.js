import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    View
} from 'react-native';

import UsernameInput from '../username-input';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.props = props;
    }

    render() {
        return <View style={styles.usernameInput}><UsernameInput onSubmit={this.props.onLogin} /></View>
    }
}

const styles = StyleSheet.create({
    usernameInput: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    }
});