import config from '../../config';

class FirebasePrototypeServer {
  async refreshDeviceToken(user, deviceToken) {
    var response = await fetch(`${config.FIREBASE_PROTOTYPE_SERVER_URL}/users/${user}/deviceToken`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({deviceToken: deviceToken})
      }
    );

    return response.ok;
  }

  async newMessage(fromUser, toUser, content) {
    let message = { 'fromUser': fromUser, 'content': content };
    var response = await fetch(`${config.FIREBASE_PROTOTYPE_SERVER_URL}/users/${toUser}/messages`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(message)
      }
    );

    return response.ok;
  }

  async getUsernames() {
    let response = await fetch(`${config.FIREBASE_PROTOTYPE_SERVER_URL}/users`);

    return await response.json();
  }
}

export default new FirebasePrototypeServer();
