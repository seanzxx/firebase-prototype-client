import React, { Component } from 'react';
import { View, ScrollView, Text, TextInput, TouchableHighlight, StyleSheet} from 'react-native';

import ConversationItem from './conversation-item';
import ContentInput from './content-input'

export default class ConversationView extends Component {
    constructor(props) {
        super(props);

        this.props = props;
    }

    render() {
        return (
          <View style={styles.container}>
            <ScrollView>
              {this.props.conversation.map((item, i)=><ConversationItem key={item.fromUser + '-' + i} fromUser={item.fromUser} content={item.content} />)}
            </ScrollView>
            <ContentInput onSubmit={this.props.onSend}/>
          </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
    }
});
