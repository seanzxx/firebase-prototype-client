import React, { Component } from 'react';
import { View, ScrollView, Text, TextInput, TouchableHighlight, StyleSheet} from 'react-native';

export default class ConversationItem extends Component {
  constructor(props) {
    super(props);

    this.props = props;
  }

  render() {
      return (
        <View style={styles.container}>
          <Text style={styles.fromUser}>{this.props.fromUser}</Text>
          <Text style={styles.content}>{this.props.content}</Text>
        </View>
      )
  }
};

const styles = StyleSheet.create({
    container: {
      flex: 0,
      padding: 10,
    },
    fromUser: {
      fontWeight: 'bold',
    },
    content: {
      flex: 0,
      marginLeft: 20,
    }
});
