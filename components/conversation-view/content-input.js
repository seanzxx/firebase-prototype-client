import React, { Component } from 'react';
import {TextInput, StyleSheet} from 'react-native';

export default class ContentInput extends Component {

  constructor(props) {
    super(props);

    this.props = props;

    this.state = {content: ''};
  }

  render() {
    return (
      <TextInput {...this.props}
        onChangeText={this.changeText}
        onSubmitEditing = {this.submitEditing}
        value={this.state.content}/>
    )
  }

  changeText = (content) => {
    this.setState({content});
  };

  submitEditing = (event) => {
    this.props.onSubmit(event.nativeEvent.text);
    this.setState({content: ''});
  }
}
