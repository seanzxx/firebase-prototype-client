A prototype for the Firebase Cloud Messaging.

# What is the Firebase Cloud Messaging

FCM is the new version of GCM under the Firebase brand. It inherits GCMâ€™s core infrastructure, with new SDKs to make Cloud Messaging development easier.

Benefits of upgrading to FCM SDK include:

- Simpler client development. You no longer have to write your own registration or subscription retry logic.
- An out-of-the-box notification solution. You can use Firebase Notifications, a serverless notifications solution with a web console that lets anyone send notifications to target specific audiences based on Firebase Analytics insights.

Source URL: https://firebase.google.com/support/faq/

# Do we need to use FCM instead of GCM?

YES.

Firebase Cloud Messaging builds on and improves the Google Cloud Messaging API. You can keep using Google Cloud Messaging with com.google.android.gms.gcm, but we **recommend** [upgrading](https://developers.google.com/cloud-messaging/android/android-migrate-fcm) to com.google.firebase:firebase-messaging.

Google Cloud Messaging (GCM) is integrated with Firebase. Existing users of GCM can continue to use GCM without interruption, though we strongly recommend upgrading to the new and simplified Firebase Cloud Messaging (FCM) APIs, so that users can benefit from future releases of new features and enhancements. 

Source URL: https://developers.google.com/android/guides/releases#may_2016_-_v90

# Is Firebase Cloud Messaging FREE?

YES.  

Firebase's paid infrastructure products **are**:  the Realtime Database, Firebase Storage, Hosting, and Test Lab. We offer a free tier for all of these products except Test Lab.

Firebase also has many **free** products: Analytics, App Indexing, Authentication, Dynamic Links, **Cloud Messaging**, **Notifications**, Invites, Crash Reporting, &, Remote Config. You can use an unlimited amount of these in all plans, including our free Spark Plan.

Source URL: https://firebase.google.com/pricing/

# Is there Enterprise contracts/pricing/support?

NO. 

Our Blaze plan is suitable for Enterprises of all sizes, and our SLA meets or exceeds the industry standard for Cloud infrastructure. However, we do not currently offer Enterprise contracts, pricing, or support, nor do we offer dedicated infrastructure hosting (i.e. on-premise installations) for services like our Realtime Database. 

Source URL: https://firebase.google.com/pricing/

# Client Integration
## iOS

https://firebase.google.com/docs/cloud-messaging/ios/client

## Android

https://firebase.google.com/docs/cloud-messaging/android/client

# See also
1. [GCM is now FCM (Firebase Cloud Messaging) - Google I/O 2016](https://www.youtube.com/watch?v=VpbNFIY1qJ0)
2. https://firebase.google.com/docs/cloud-messaging/notifications
