/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import {AppRegistry} from 'react-native';

import FirebasePrototypeApp from './components/firebase-prototype-app';

AppRegistry.registerComponent('FirebasePrototype', () => FirebasePrototypeApp);
