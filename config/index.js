import FirebasePrototypeServerConfig from './firebase-prototype-server';

export default {
  ...FirebasePrototypeServerConfig
};
